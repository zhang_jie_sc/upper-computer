﻿#include "TakeSampleCommand.h"
#include "GlobalVar.h"
#include "WaitUtils.h"
#include "NotificationManager.h"

TakeSampleCommand::TakeSampleCommand(QObject *parent) : BaseCommand(parent)
{

}

void TakeSampleCommand::Execute()
{
    _ioModule->GetIOItem("取样阀")->SetStatusDelayReset(true,30);
    _ioModule->GetIOItem("取样蠕动泵")->SetStatusDelayReset(true,30);

    for(int i=0;i<100;i++)
    {
        WaitUtils::WaitMs(300);
        NotificationManager::Instance().UpdateProgress(i);
    }
}

void TakeSampleCommand::SetDevices(QList<BaseDevice *> devices)
{
    BaseCommand::SetDevices(devices);
    _ioModule=dynamic_cast<BaseIOModule*>(devices.at(0));

    if(_ioModule==nullptr)
    {
        throw QString("TakeSampleCommand 配置失败,IO模块配置错误!");
    }
}
