﻿#include "CommandFactory.h"
#include <QDebug>

#include "TakeSampleCommand.h"
#include "InSampleCommand.h".h"

CommandFactory::CommandFactory()
{
    Register<TakeSampleCommand>();
    Register<InSampleCommand>();
}
