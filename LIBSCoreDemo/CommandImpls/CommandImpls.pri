INCLUDEPATH += $$PWD

include($$PWD/CommandFactory/CommandFactory.pri)

HEADERS += \
    $$PWD/InSampleCommand.h \
    $$PWD/TakeSampleCommand.h

SOURCES += \
    $$PWD/InSampleCommand.cpp \
    $$PWD/TakeSampleCommand.cpp
