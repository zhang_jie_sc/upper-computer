﻿#include "InSampleCommand.h"
#include "WaitUtils.h"
InSampleCommand::InSampleCommand(QObject *parent) : BaseCommand(parent)
{

}

void InSampleCommand::Execute()
{
    for(int i=0;i<100;i++)
    {
        WaitUtils::WaitMs(300);
        NotificationManager::Instance().UpdateProgress(i);
    }
}

void InSampleCommand::SetDevices(QList<BaseDevice *> devices)
{

}
