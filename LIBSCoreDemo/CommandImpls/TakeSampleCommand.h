﻿#ifndef TAKESAMPLECOMMAND_H
#define TAKESAMPLECOMMAND_H
#include "Head.h"
#include <QObject>
#include "BaseIOModule.h"
#include "BaseCommand.h"

class TakeSampleCommand : public BaseCommand
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit TakeSampleCommand(QObject *parent = nullptr);

signals:

public slots:

    // BaseCommand interface
public:
    virtual void Execute() override;
    virtual void SetDevices(QList<BaseDevice *> devices) override;

private:
    BaseIOModule *_ioModule=nullptr;
};

#endif // TAKESAMPLECOMMAND_H
