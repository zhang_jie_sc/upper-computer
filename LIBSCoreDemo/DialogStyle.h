﻿#ifndef DIALOGSTYLE_H
#define DIALOGSTYLE_H
#include "Head.h"
#include <QDialog>

namespace Ui {
class DialogStyle;
}

class DialogStyle : public QDialog
{
    Q_OBJECT

public:
    explicit DialogStyle(QWidget *parent = 0);
    ~DialogStyle();

private slots:
    void on_pushButton_clicked();

private:
    Ui::DialogStyle *ui;
};

#endif // DIALOGSTYLE_H
