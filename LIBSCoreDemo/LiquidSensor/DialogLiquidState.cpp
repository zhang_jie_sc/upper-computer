﻿#include "DialogLiquidState.h"
#include "ui_DialogLiquidState.h"

DialogLiquidState::DialogLiquidState(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogLiquidState)
{
    ui->setupUi(this);
}

DialogLiquidState::~DialogLiquidState()
{
    qDebug()<<__FUNCTION__<<__LINE__;
    delete ui;
}

void DialogLiquidState::SetLiquidSensor(BaseLiquidSensor *sensor)
{
    _sensor=sensor;
}


void DialogLiquidState::on_pushButton_clicked()
{
     LiquidState state=_sensor->GetState();
     ui->txtState->setText(QString::number(state.State));
     ui->txtRSSI->setText(QString::number(state.RSSI));
}
