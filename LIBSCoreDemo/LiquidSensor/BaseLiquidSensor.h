﻿#ifndef BASELIQUIDSENSOR_H
#define BASELIQUIDSENSOR_H

#include <QObject>
#include "BaseDevice.h"
#include "BaseModbus.h"

struct LiquidState
{
    LiquidState()
    {
        State=false;
        RSSI=0;
    }
    bool State;
    int RSSI;
};

class BaseLiquidSensor : public BaseDevice
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit BaseLiquidSensor(QObject *parent = nullptr);

    virtual LiquidState GetState();

signals:

public slots:

    // BaseItem interface
public:
    virtual QWidget *GetConfigWidget() override;
};

#endif // BASELIQUIDSENSOR_H
