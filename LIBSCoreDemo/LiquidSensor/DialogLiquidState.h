﻿#ifndef DIALOGLIQUIDSTATE_H
#define DIALOGLIQUIDSTATE_H

#include <QDialog>
#include "BaseLiquidSensor.h"

namespace Ui {
class DialogLiquidState;
}

class DialogLiquidState : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLiquidState(QWidget *parent = 0);
    ~DialogLiquidState();

    void SetLiquidSensor(BaseLiquidSensor *sensor);

private slots:
    void on_pushButton_clicked();

private:
    Ui::DialogLiquidState *ui;
    BaseLiquidSensor *_sensor=nullptr;
};

#endif // DIALOGLIQUIDSTATE_H
