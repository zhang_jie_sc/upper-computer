﻿#include "BaseLiquidSensor.h"
#include "DialogLiquidState.h"

BaseLiquidSensor::BaseLiquidSensor(QObject *parent) : BaseDevice(parent)
{

}

LiquidState BaseLiquidSensor::GetState()
{
    return LiquidState();
}

QWidget *BaseLiquidSensor::GetConfigWidget()
{
    DialogLiquidState *d=new DialogLiquidState();
    d->SetLiquidSensor(this);
    return d;
}
