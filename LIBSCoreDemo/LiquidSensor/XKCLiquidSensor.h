﻿#ifndef XKCLIQUIDSENSOR_H
#define XKCLIQUIDSENSOR_H

#include <QObject>
#include "BaseModbus.h"
#include "BaseLiquidSensor.h"

//深圳星科创液位传感器
class XKCLiquidSensor : public BaseLiquidSensor
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit XKCLiquidSensor(QObject *parent = nullptr);

signals:

public slots:

    // BaseDevice interface
public:
    virtual void SetCommunication(QList<BaseCommunication *> communication) override;

protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;

    // BaseLiquidSensor interface
public:
    virtual LiquidState GetState() override;

private:
    BaseModbus *_modbus=nullptr;
    LiquidState _state;
};

#endif // XKCLIQUIDSENSOR_H
