﻿#include "SingleMeasureController.h"
#include "CommandFactory.h"

SingleMeasureController::SingleMeasureController(QObject *parent):BaseController(parent)
{

}

void SingleMeasureController::Execute()
{
   BaseController::ExecuteWrap(_allCommands);
}


void SingleMeasureController::SetConfig(QJsonObject obj)
{
    BaseController::SetConfig(obj);
    _mgr=new CommandManager(obj,new CommandFactory());
    _mgr->InitConfig();

    _allCommands.clear();
    _allCommands.append(_mgr->GetItems<BaseCommand>());
}
