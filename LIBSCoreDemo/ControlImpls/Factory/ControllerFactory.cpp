﻿#include "ControllerFactory.h"
#include "SingleMeasureController.h"

ControllerFactory::ControllerFactory()
{
    Register<SingleMeasureController>();
}
