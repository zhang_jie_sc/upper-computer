﻿#ifndef CONTROLLERFACTORY_H
#define CONTROLLERFACTORY_H

#include <QObject>
#include <QMap>
#include "BaseCommand.h"
#include "BaseFactory.h"

class ControllerFactory : public BaseFactory
{
public:
    explicit ControllerFactory();
};

#endif // CONTROLLERFACTORY_H
