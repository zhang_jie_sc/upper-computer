﻿#ifndef GLOBALVAR_H
#define GLOBALVAR_H
#include <QObject>
#include <QVariant>
#include <QDateTime>
#include <QSettings>


struct AutoFocusParam
{
    AutoFocusParam()
    {
        FocusPos=0;
    }
    double FocusPos;
};

struct ControlParam
{
    ControlParam()
    {
        TakeSampleDurationSecond=10;
        TakeSamplePumpDurationSecond=10;
        InWaterDurationSecond=10;
        AutoInSampleDurationSecond=10;
        ManualInSampleDurationSecond=10;
        InSamplePumpDurationSecond=10;
        OutSampleDurationSecond=10;
        BlowDurationSecond=10;
    }
    int TakeSampleDurationSecond;
    int TakeSamplePumpDurationSecond;
    int InWaterDurationSecond;
    int AutoInSampleDurationSecond;
    int ManualInSampleDurationSecond;
    int InSamplePumpDurationSecond;
    int OutSampleDurationSecond;
    int BlowDurationSecond;
};

//struct ModelParam
//{
//    ModelParam()
//    {
//        FocusPos=0;
//    }
//    double k1;
//};

class GlobalVar:public QObject
{
    Q_OBJECT
private:
    GlobalVar(QObject *parent=nullptr);
    GlobalVar(const GlobalVar&)=delete;
    GlobalVar& operator=(const GlobalVar&)=delete;

signals:
    void ConfigChanged();
    void OnMonitorParamUpdate();

public:
    static GlobalVar& Instance();

    QString DeviceType;
    QSettings* Setting;

    ControlParam GetControlParam();
    void SetControlParam(ControlParam param);
};

#endif // GLOBALVAR_H
