﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Head.h"
#include <QMainWindow>
#include <QProcess>
#include <QSystemTrayIcon>
#include <QLayout>
#include <QGroupBox>
#include <QLabel>

#include "BaseIOModule.h"
#include "BaseMotorController.h"
#include "Interrupter.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:

    void on_btnStopColl_clicked();

    void on_btnOpenDir_clicked();

    void on_btnOpenFile_clicked();

    void Log(QString log);

    void on_actionUpdateLog_triggered();

    void on_actionConfigFile_triggered();

    void CloseWindow();

    void on_actionSetStyle_triggered();

    void on_actionIOModule_triggered();

    void on_actionConfig_triggered();

    void on_btnStart_clicked();

    void on_btnStop_clicked();

    void OnHandleInstrumentStatusChanged(QString title, int progress);

    void on_btnRunSingle_clicked();

    void on_actionLiquid_triggered();

private:
    BaseIOModule *_ioModule=nullptr;
    Interrupter _interrupter;
    QDialog *_motorW=nullptr;

    QString ReadAll(QProcess &process);
    QMap<QString, double> GetValue(QString input);

    QSystemTrayIcon  *trayIcon=nullptr;
    QSystemTrayIcon *CreateTrayIcon(QString title);

    void InitGroupBox(QStringList labels, QGroupBox *gb);
    void AddIOLabel(QString label, QGroupBox *parent);

    void OnStateUpdateHandle(DeviceStateItems items);

    void UpdateStyle();
    
protected:
    virtual void timerEvent(QTimerEvent *event) override;

    virtual void closeEvent(QCloseEvent *event) override;

    QMap<QString,QLabel*> _deviceMap;
};

#endif // MAINWINDOW_H
