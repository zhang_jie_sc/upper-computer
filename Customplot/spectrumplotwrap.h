﻿#ifndef CUSTOMPLOTWRAP_H
#define CUSTOMPLOTWRAP_H
#include "Head.h"
#include <QPointF>
#include <QObject>
#include <QSettings>
#include "qcustomplot.h"

class SpectrumPlotWrap:public QObject
{
    Q_OBJECT
public:
    SpectrumPlotWrap(QCustomPlot *plot,QSettings *set=nullptr);

    void SetXYLabel(QString xLabel,QString yLabel);

    void SetGraph(QStringList names);

    void Plot(const QList<QList<QPointF>> &items);

    void Plot(const QList<QPointF> &item, int index);

    void Clear(int index);

    void SetTitle(QString title);

    void SetColors(QList<QColor> colors);

    int GraphCount();

    void plotXY(const QList<double> &x, const QList<double> &y, int index, const QString &lineType = "l");

public slots:
    void mousePress();
    void mouseWheel();
private slots:
    void ShowInitDialog();

private:
    QCustomPlot* _plot;
    QCPTextElement* _title;
    QSettings *_set=nullptr;
    bool _lockAxis=false;
    double _upper=2000;
    void SetPlotConfig(bool lockAxis, double upper);
    void GetPlotConfig(bool &lockAxis, double &upper);
};

#endif // CUSTOMPLOTWRAP_H
