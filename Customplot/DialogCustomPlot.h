﻿#ifndef DIALOGCUSTOMPLOT_H
#define DIALOGCUSTOMPLOT_H

#include <QDialog>

namespace Ui {
class DialogCustomPlot;
}

class DialogCustomPlot : public QDialog
{
    Q_OBJECT

public:
    explicit DialogCustomPlot(bool &isLock,double &upper,QWidget *parent = 0);
//    explicit DialogCustomPlot(QWidget *parent = 0);
    ~DialogCustomPlot();

private:
    Ui::DialogCustomPlot *ui;
    bool &_isLock;
    double &_upper;

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *event) override;
};

#endif // DIALOGCUSTOMPLOT_H
