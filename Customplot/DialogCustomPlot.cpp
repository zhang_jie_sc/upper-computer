﻿#include "DialogCustomPlot.h"
#include "ui_DialogCustomPlot.h"

DialogCustomPlot::DialogCustomPlot(bool &isLock, double &upper, QWidget *parent):
    QDialog(parent),
    ui(new Ui::DialogCustomPlot),_isLock(isLock),_upper(upper)
{
    ui->setupUi(this);
    ui->checkBox->setChecked(_isLock);
    ui->doubleSpinBox->setValue(_upper);
}

DialogCustomPlot::~DialogCustomPlot()
{
    delete ui;
}

void DialogCustomPlot::closeEvent(QCloseEvent *event)
{
    QDialog::closeEvent(event);
    _isLock=ui->checkBox->isChecked();
    _upper=ui->doubleSpinBox->value();
}
