﻿#ifndef CMDMANAGER_H
#define CMDMANAGER_H
#include "Head.h"
#include <QObject>
#include "BaseManager.h"

class CommandManager : public BaseManager
{
    Q_OBJECT
public:
    CommandManager(QJsonObject obj,BaseFactory *factory,QObject *parent = nullptr);
    CommandManager(const CommandManager&)=delete;
    CommandManager& operator =(const CommandManager&)=delete;
    ~CommandManager();

    void InitConfig() override final;

signals:

public slots:

protected:
    virtual void InitImpl(QJsonObject obj) override;
};

#endif // CMDMANAGER_H
