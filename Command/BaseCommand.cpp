﻿#include "BaseCommand.h"


BaseCommand::BaseCommand(QObject *parent):BaseItem(parent)
{
}

void BaseCommand::Execute()
{

}

QString BaseCommand::GetFriendlyName()
{
    return _obj["FriendlyName"].toString();
}

int BaseCommand::GetWeight()
{
    return _other["Weight"].toInt();
}

void BaseCommand::SetDevices(QList<BaseDevice *> devices)
{
    _allDevices=devices;
}

QList<BaseCommand *> BaseCommand::GetSubCommands()
{
    return QList<BaseCommand*>();
}

void BaseCommand::SetConfig(QJsonObject obj)
{
    BaseItem::SetConfig(obj);
    auto devices=DeviceManager::Instance().GetDevices(obj["Devices"].toArray());
    SetDevices(devices);
}
