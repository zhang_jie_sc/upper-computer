﻿#ifndef BASECOMMANDFACTORY_H
#define BASECOMMANDFACTORY_H

#include <QMap>
#include "BaseCommand.h"
#include "BaseFactory.h"

class BaseCommandFactory:public BaseFactory
{
public:
    BaseCommandFactory();
};

#endif // BASECOMMANDFACTORY_H
