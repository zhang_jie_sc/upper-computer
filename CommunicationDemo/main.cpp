﻿#include <QApplication>
#include <iostream>
#include <QDebug>

#include "mainwindowcomm.h"
#include "GlobalVar.h"
#include "logger.h"
#include "apputils.h"

int runGuiMode(int argc, char *argv[])
{
    QApplication app(argc, argv);

    if(AppUtils::IsShareMemoryOccupy("LIBS2700YEControl",true))
    {
        return 1;
    }

    QApplication::setWindowIcon(QIcon(":/icon/电瓶电解液-1种尺寸.ico"));
    QApplication::setQuitOnLastWindowClosed(false);
    MainWindowComm w;
    w.show();

    return app.exec();
}

int main(int argc, char *argv[])
{
    // 检查命令行参数
    qInstallMessageHandler(Logger::CustomMessageHandler);

    // 如果没有参数，运行 GUI 模式
    return runGuiMode(argc, argv);
}
