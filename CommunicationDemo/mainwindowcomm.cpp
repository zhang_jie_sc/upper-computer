#include "mainwindowcomm.h"
#include "ui_mainwindowcomm.h"
#include "formrequester.h"

MainWindowComm::MainWindowComm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowComm)
{
    ui->setupUi(this);

    for(int i=0;i<5;i++)
    {
        ui->tabWidget->addTab(new FormRequester(),"requester");
    }

}

MainWindowComm::~MainWindowComm()
{
    delete ui;
}
