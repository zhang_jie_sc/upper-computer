#ifndef MAINWINDOWCOMM_H
#define MAINWINDOWCOMM_H

#include <QMainWindow>

namespace Ui {
class MainWindowComm;
}

class MainWindowComm : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowComm(QWidget *parent = 0);
    ~MainWindowComm();

private:
    Ui::MainWindowComm *ui;
};

#endif // MAINWINDOWCOMM_H
