#ifndef FORMREQUESTER_H
#define FORMREQUESTER_H

#include <QWidget>

namespace Ui {
class FormRequester;
}

class FormRequester : public QWidget
{
    Q_OBJECT

public:
    explicit FormRequester(QWidget *parent = 0);
    ~FormRequester();

private:
    Ui::FormRequester *ui;
};

#endif // FORMREQUESTER_H
