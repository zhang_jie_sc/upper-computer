﻿#ifndef JOSNUTILS_H
#define JOSNUTILS_H
#include "Head.h"
#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QSettings>
#include <QTextCodec>

class JsonUtils : public QObject
{
    Q_OBJECT

public:
    explicit JsonUtils(QObject *parent = nullptr);

    static bool LoadJsonObject(const QString title,QJsonObject &jsonObj);

    static bool SaveJsonObject(const QString title,QJsonObject &jsonObj);

    static QJsonObject LoadMainConfig();

    static QString LoadKey();

    static bool ValidKey(QString serialId);

    static QJsonObject LoadJsonFile(QString path,bool isEncrypt=true);

    static bool SaveJsonFile(const QString &fn,const QJsonObject &jsonObj,bool isEncrypt=true);

    static QString ConvertToJsonStr(QJsonObject jsonObj);

    static QString ConvertTOStr(QJsonObject jsonObj);

    static QJsonObject ConvertToJObj(QString jsonStr);

    static QString GetFormatFilePath(QString title);

    static bool IsExist(QString title);

private:
    static bool TryCreateFile(const QJsonObject &jsonObj, QString fn);

    static QString GetDeviceType();
    
signals:

public slots:
};



#endif // JOSNUTILS_H
