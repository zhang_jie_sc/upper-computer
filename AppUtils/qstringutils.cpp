﻿#include "qstringutils.h"
#include <QDebug>

QStringUtils::QStringUtils(QObject *parent) : QObject(parent)
{

}

QString QStringUtils::MapToString(QMap<QString, double> map)
{
    QString ret;
    for(auto key:map.keys())
    {
        ret.append(QString("%1:%2 ").arg(key).arg(map[key]));
    }
    return ret;
}

char *QStringUtils::QStringToCStrArray(QString source)
{
    char* charArray = new char[source.length() + 1]; // 字符数组，大小为字符串长度加 1

    for (int i = 0; i < source.length(); ++i) {
        charArray[i] = source.at(i).toLatin1(); // 将 QString 中的字符逐个复制到字符数组中
    }
    charArray[source.length()] = '\0'; // 添加终止字符 '\0'

    return charArray;
}

void QStringUtils::QStringCpoyCStrArray(QString source, char target[],int length)
{
    qstrncpy(target, source.toUtf8().constData(), length);
    target[length-1] = '\0'; // 添加终止字符 '\0'
}
