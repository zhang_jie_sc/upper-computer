﻿#include "JsonUtils.h"
#include <QDebug>
#include <QDir>
#include <QFile>
#include "Crypto.h"

JsonUtils::JsonUtils(QObject *parent) : QObject(parent)
{}

QJsonObject JsonUtils::ConvertToJObj(QString jsonStr)
{
    QJsonDocument jsonDoc(QJsonDocument::fromJson(jsonStr.toUtf8()));
    return jsonDoc.object();
}

bool JsonUtils::LoadJsonObject(const QString title, QJsonObject &jsonObj)
{
    QString deviceType=GetDeviceType();
    QString dirS="./Config/"+deviceType;
    QString fn=dirS+"/"+title+".json";
    QJsonObject loadObj=LoadJsonFile(fn);
    jsonObj=loadObj;
    return true;
}

bool JsonUtils::SaveJsonObject(const QString title, QJsonObject &jsonObj)
{
    QString fn="./Config/"+GetDeviceType()+"/"+title+".json";
    return SaveJsonFile(fn,jsonObj);
}

QJsonObject JsonUtils::LoadMainConfig()
{
    QJsonObject mainObj=LoadJsonFile("./Config/Main.json",false);
    return mainObj;
}

QString JsonUtils::LoadKey()
{
    QJsonObject mainObj=LoadMainConfig();
    QString encryptText=mainObj["Key"].toString();
    if(encryptText=="gszj")
    {
        return encryptText;
    }
    Crypto crypto("gs");
    return crypto.decrypt(encryptText);
}

bool JsonUtils::ValidKey(QString serialId)
{
    QString key=LoadKey();
    if(key=="gszj")
    {
        return true;
    }
    else
    {
        qDebug()<<__FUNCTION__<<__LINE__<<serialId<<key;
        return serialId==key;
    }
}

QString JsonUtils::GetDeviceType()
{
    QJsonObject mainObj=LoadMainConfig();
    QString deviceType=mainObj["DeviceType"].toString();
    return deviceType;
}

QString JsonUtils::ConvertToJsonStr(QJsonObject jsonObj)
{
    QJsonDocument jsonDocument(jsonObj);
    return jsonDocument.toJson();
}

QString JsonUtils::ConvertTOStr(QJsonObject jsonObj)
{
    QStringList keyValuePairs;

    // 迭代QJsonObject中的每个键值对
    for (auto it = jsonObj.begin(); it != jsonObj.end(); ++it) {
        QString key = it.key();
        QJsonValue value = it.value();

        // 根据值的类型进行适当的处理
        if (value.isString()) {
            keyValuePairs.append(QString("%1:%2").arg(key, value.toString()));
        } else if (value.isDouble() || value.isBool()) {
            keyValuePairs.append(QString("%1:%2").arg(key, value.toVariant().toString()));
        } else {
            // 处理其他类型，如果需要
        }
    }

    // 使用逗号分隔每个键值对
    return keyValuePairs.join(",");
}

bool JsonUtils::IsExist(QString title)
{
    QString fn="./Config/"+GetDeviceType()+"/"+title+".json";
    QFile f(fn);
    return f.exists();
}

QString JsonUtils::GetFormatFilePath(QString title)
{
    QString fn="./Config/"+GetDeviceType()+"/"+title+".json";
    return fn;
}

bool JsonUtils::TryCreateFile(const QJsonObject &jsonObj, QString fn)
{
    QFile jsonFile(fn);

    if(!jsonFile.exists())
    {
        // 创建Json文档
        jsonFile.open(QIODevice::WriteOnly);
        QJsonDocument doc(jsonObj);
        jsonFile.write(doc.toJson());
        jsonFile.close();
    }
    return true;
}

QJsonObject JsonUtils::LoadJsonFile(QString fn,bool isEncrtpt)
{
    QFile jsonFile(fn);
    if (!jsonFile.open(QIODevice::ReadOnly))
    {
        throw QString("%打开失败!").arg(fn);
    }

    QByteArray fileData = jsonFile.readAll();
    jsonFile.close();

    QString fileText = QString::fromUtf8(fileData);
    Crypto crypto("gs");
    if (crypto.isEncrypted(fileText)) {
        fileText = crypto.decrypt(fileText);
    }
    else if(isEncrtpt){
//        QJsonDocument jd(QJsonDocument::fromJson(fileText.toUtf8()));
//        SaveJsonFile(fn,jd.object());
    }

    QJsonDocument jsonDoc(QJsonDocument::fromJson(fileText.toUtf8()));
    return jsonDoc.object();
}

bool JsonUtils::SaveJsonFile(const QString &fn, const QJsonObject &jsonObj,bool isEncrypt)
{
    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = QString::fromUtf8(jsonDoc.toJson(QJsonDocument::Indented));

    if(isEncrypt)
    {
        Crypto crypto("gs");
        jsonString = crypto.encrypt(jsonString);
    }


    QFile jsonFile(fn);
    if (!jsonFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        // Handle error
        return false;
    }

    jsonFile.write(jsonString.toUtf8());
    jsonFile.close();
    return true;
}

