﻿#include "timermanager.h"

TimerManager::TimerManager(QObject *parent) : QObject(parent)
{

}

TimerManager &TimerManager::Instance()
{
    static TimerManager _instance;
    return _instance;
}

QTimer* TimerManager::CreatSingleSloteTimer(int intervalMs)
{
    QTimer *ret=new QTimer();
    ret->setInterval(intervalMs);
    ret->setSingleShot(true);
    _allTimers.append(ret);
    return ret;
}

bool TimerManager::StopAllTimers()
{
    for(QTimer *timer:_allTimers)
    {
        timer->stop();
        delete timer;
    }
    _allTimers.clear();
    return true;
}


