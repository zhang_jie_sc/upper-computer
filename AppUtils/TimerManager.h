﻿#ifndef TIMERMANAGER_H
#define TIMERMANAGER_H

#include <QObject>
#include <QTimer>

class TimerManager : public QObject
{
    Q_OBJECT
private:
    explicit TimerManager(QObject *parent = nullptr);

    TimerManager(const TimerManager&)=delete;
    TimerManager& operator=(const TimerManager&)=delete;

public:
    static TimerManager& Instance();

    QTimer* CreatSingleSloteTimer(int intervalMs);

    bool StopAllTimers();

private:
    QList<QTimer*> _allTimers;

signals:

public slots:
};

#endif // TIMERMANAGER_H
