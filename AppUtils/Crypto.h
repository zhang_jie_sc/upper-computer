﻿#ifndef CRYPTO_H
#define CRYPTO_H

#include <QString>
#include <QByteArray>

class Crypto
{
public:
    explicit Crypto(const QString &key);
    QString encrypt(const QString &plainText);
    QString decrypt(const QString &cipherText);
    bool isEncrypted(const QString &text);

private:
    QByteArray key;
    QByteArray applyXor(const QByteArray &data);
};

#endif // CRYPTO_H


