﻿#include <algorithm>
#include <QIODevice>
#include <QDataStream>
#include <QVector>
#include "bitutils.h"

BitUtils::BitUtils()
{

}

QList<uint16_t> BitUtils::ConvertToUint16List(int data)
{
    QList<uint16_t> ret;
    ret.append(data >> 16);//高位
    ret.append(data & 0xFFFF);//低位
    return ret;
}

QList<uint8_t> BitUtils::ConvertToUint8List(uint16_t data)
{
    QList<uint8_t> ret;
    ret.append(data >> 8);//高位
    ret.append(data & 0xFF);//低位
    return ret;
}

float BitUtils::ArrayToFloat(QByteArray data)
{
    data=data.toHex();
    if(8 != data.count()) {
        return -1;
    }
    QByteArray tem;
    for(int i = 3; i>=0; i--) {
        tem.append(data.at(2*i));
        tem.append(data.at(2*i + 1));
    }
    uint hex_uint = tem.toUInt(0,16);
    float hex_res=-1;
    std::memcpy(&hex_res, &hex_uint, sizeof(float)); // 将二进制数据复制到浮点数
    return hex_res;
}

QByteArray BitUtils::FloatToArray(float value)
{
    QByteArray byteArray;
    QDataStream stream(&byteArray, QIODevice::WriteOnly);
    stream.setFloatingPointPrecision(QDataStream::SinglePrecision);
    stream << value;
    std::reverse(byteArray.begin(),byteArray.end());
    return byteArray;
}

int BitUtils::ArrayToInt(QByteArray data)
{
    std::reverse(data.begin(),data.end());
    return data.toHex().toInt(0,16);
}

QByteArray BitUtils::IntToArray(int value)
{
    QByteArray byteArray;
    byteArray.append(static_cast<char>((value >> 0) & 0xFF));
    byteArray.append(static_cast<char>((value >> 8) & 0xFF));
    byteArray.append(static_cast<char>((value >> 16) & 0xFF));
    byteArray.append(static_cast<char>((value >> 24) & 0xFF));
    return byteArray;
//    QByteArray byteArray;
//    QDataStream stream(&byteArray, QIODevice::ReadWrite);
//    stream<<value;
//    std::reverse(byteArray.begin(),byteArray.end());
//    return byteArray;
}

QList<uint8_t> BitUtils::Uint32ConvertToUint8List(uint32_t data)
{
    QList<uint8_t> ret;
    ret.append(data >> 24);//高位
    ret.append(data >> 16);//高位
    ret.append(data >> 8);//高位
    ret.append(data & 0xFF);//低位
    return ret;
}

QList<uchar> BitUtils::Uint32ConvertToUCharList(uint32_t data)
{
    QList<uchar> ret;
    foreach (uint8_t value, Uint32ConvertToUint8List(data)) {
        ret.append(static_cast<uchar>(value));
    }
    return ret;
}


QList<uchar> BitUtils::SumCheck(QList<uchar> data)
{
    int sum = 0;
    for(int i = 0; i < data.count(); i++) {
        sum += data.at(i);
    }
    return ConvertToUint8List(sum);
}

int BitUtils::ConvertToInt(QList<uint16_t> data)
{
    int data0=data.at(0);
    return (data0 << 16) + data.at(1);
}

bool BitUtils::GetBit(ushort b, int bitNumber)
{
    if (bitNumber < 0 || bitNumber > 15)
    {
        throw QString("bitNumber 超出索引 %1").arg(bitNumber);
    }
    return (b & (1 << bitNumber)) >= 1;
}

uint16_t BitUtils::GetNum(const QVector<bool> &bArr)
{
    uint16_t result = 0;

    for (int i = 0; i < bArr.size() && i < 16; ++i)
    {
        if (bArr.at(i))
        {
            // 如果当前布尔值为true，则将对应的位设置为1
            result |= static_cast<uint16_t>(1 << i);
        }
    }
    return result;
}


QString BitUtils::HexToString(QByteArray data)
{
    QString hexString = data.toHex();
    QString formattedString;
    for (int i = 0; i < hexString.length(); i += 2) {
        formattedString +=  hexString.mid(i, 2) + " ";
    }
    return formattedString;

}
