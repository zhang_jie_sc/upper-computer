﻿#pragma execution_character_set("utf-8")
#ifndef WIDGETUTILSNEW_H
#define WIDGETUTILSNEW_H
#include <QWidget>
#include <functional>
#include <QDebug>

namespace WidgetUtils {

    template<typename Func>
    bool Execute(QObject *widget,Func func,bool showExMsg=true)
    {
        QWidget* control=widget==nullptr?nullptr:dynamic_cast<QWidget*>(widget);
        bool ret=false;
        try
        {
            if(control!=nullptr)
            {
                control->setEnabled(false);
            }
            func();
            ret=true;
        }
        catch(QString ex)
        {
            if(showExMsg)
            {
                WidgetUtilsNew::ShowWarning(ex);
            }
            qDebug()<<ex;
        }
        if(control!=nullptr)
        {
            control->setEnabled(true);
        }
        return ret;
    }

    class WidgetUtilsNew
    {
    public:
        WidgetUtilsNew();
        static void ShowWarning(const QString &warningText);
    };

}


#endif // WIDGETUTILSNEW_H
