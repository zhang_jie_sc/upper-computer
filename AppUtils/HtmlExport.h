﻿#ifndef HTMLEXPORT_H
#define HTMLEXPORT_H
#include "Head.h"
#include <QObject>

class HtmlExport : public QObject
{
    Q_OBJECT
public:
    explicit HtmlExport(QObject *parent = nullptr);

    void SetMeasureParam(QStringList headers,QStringList values);

    void SetMeasureResult(QStringList headers,QStringList values);

    void SetEachMeasureResult(QStringList headers,QList<QStringList> values);

    QString Export(QString templateFn,QString targetFn);

    // 函数：读取并修改HTML模板
    static QString ReadAndModifyTemplate(QString templatePath, QString measureParamHeader,QString measureParamValue);

    static QString GenerateTableHeader(QStringList Headers);

    static QString GenerateTableRow(QStringList values);

    // 函数：读取模板文件
    static QString ReadTemplate(const QString& templatePath);

private:
    QString _measureParamHeader;
    QString _measureParamValue;
    QString _measureResultHeader;
    QString _measureResultValue;
    QString _measureEachResultHeader;
    QString _measureEachResultValue;


signals:

public slots:
};

#endif // HTMLEXPORT_H
