﻿#ifndef QPROCRESSPRO_H
#define QPROCRESSPRO_H
#include "Head.h"
#include <QObject>
#include <QProcess>

class QProcessPro : public QObject
{
    Q_OBJECT
public:
    explicit QProcessPro(QString exePath,QStringList arguments,QObject *parent = nullptr);

    void SetCode(QString code_);

    void Execute();

    void ExecuteNoWait();

    QStringList GetPrintLog();

    bool IsRunning();

signals:

public slots:
    void PrintProcessLog(QProcess &process);

private:
    QProcess _process;
    QStringList _printItems;
    QString _code;
    QString ReadAll(QProcess &process);
};

#endif // QPROCRESSPRO_H
