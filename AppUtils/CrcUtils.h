﻿#ifndef CRCUTIL_H
#define CRCUTIL_H

#include <QObject>
#include <QList>

class CRCUtils : public QObject
{
    Q_OBJECT
public:
    explicit CRCUtils(QObject *parent = nullptr);

    static unsigned short CRC16_Check(unsigned char *nData, unsigned short wLength);
    static QList<char> Parase(unsigned short incrc16);
    static QByteArray ParaseByteArray(unsigned short incrc16);
signals:

public slots:
};

#endif // CRCUTIL_H
