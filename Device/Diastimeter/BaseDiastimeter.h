﻿#ifndef BASEDIASTIMETER_H
#define BASEDIASTIMETER_H
#include "BaseDevice.h"
typedef enum EOpenModeTag{
    eMeasMode_Min = -1,
    eMeasMode_Close ,
    eMeasMode_Single ,
    eMeasMode_Continuous ,
    eMeasMode_DelayAutoMeas ,
    eMeasMode_Max
}EOpenMode;

class BaseDiastimeter:public BaseDevice
{
    Q_OBJECT
public:
    Q_INVOKABLE BaseDiastimeter(QObject *parent=nullptr);

    virtual bool SetMeasureMode(EOpenModeTag measureMode);
    virtual bool GetDiastimeterValue(double &value);
    //一般在虚拟模式中使用
    bool SetDiastimeterValue(double value);

    // BaseDevice interface
public:
    QWidget *GetConfigWidget() override final;

private:
    double _value=0;

    // BaseDevice interface
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;
};

#endif // BASEDIASTIMETER_H
