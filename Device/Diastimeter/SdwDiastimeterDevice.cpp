﻿#include "SdwDiastimeterDevice.h"

SdwDiastimeterDevice::SdwDiastimeterDevice(QObject *parent):BaseDiastimeter(parent)
{

}

bool SdwDiastimeterDevice::SetMeasureMode(EOpenModeTag measureMode)
{
    uint16_t data=static_cast<uint16_t>(measureMode);
    return _modbus->WriteRegisters(17,1,QList<uint16_t>()<<data);
}

bool SdwDiastimeterDevice::GetDiastimeterValue(double &value)
{
    QList<uint16_t> ret;
    bool isOk=_modbus->ReadOutRegisters(21,2,ret);
    if(ret.count()==2)
    {
        int retStstus=(ret.at(0)*65535+ret.at(1));
        double val=((double)retStstus)/1000;
        value=val;
    }
    return isOk;
}

void SdwDiastimeterDevice::SetCommunication(QList<BaseCommunication *> communication)
{
    BaseDiastimeter::SetCommunication(communication);
    _modbus=dynamic_cast<BaseModbus*>(communication.at(0));
}
