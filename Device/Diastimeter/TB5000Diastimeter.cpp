﻿#include <QString>
#include "TB5000Diastimeter.h"


TB5000Diastimeter::TB5000Diastimeter(QObject *parent) : BaseDiastimeter(parent)
{

}

bool TB5000Diastimeter::GetDiastimeterValue(double &value)
{
    uint16_t ret;
    bool isOk=_modbus->ReadOutRegister(0,ret);
    if(isOk)
    {
        double val=((double)ret)/2000;
        value=val;
    }
    return isOk;
}

void TB5000Diastimeter::SetCommunication(QList<BaseCommunication *> communication)
{
    BaseDiastimeter::SetCommunication(communication);
    _modbus=dynamic_cast<BaseModbus*>(communication.at(0));
    qDebug()<<__FUNCTION__<<__LINE__<<_modbus;
}

void TB5000Diastimeter::GetConnectStateCore(DeviceStateItems &items)
{
    double val=-1;
    GetDiastimeterValue(val);
    items[0].Data=QString::number(val);
}


