﻿#ifndef BASEIOMODULE_H
#define BASEIOMODULE_H
#include <QMap>
#include "BaseDevice.h"
#include "IOItem.h"
class IOItem;

//基本上都使用modbus通讯，提供下游IO设置的开关控制，可设置不同控制的IOItem
class BaseIOModule:public BaseDevice
{
    Q_OBJECT

public:
    Q_INVOKABLE BaseIOModule(QObject *parent=nullptr);
    QList<IOItem*> GetAllIOItems();

    virtual bool ReadStatus(QString ioName,bool &status);
    virtual bool WriteStatus(QString ioName,bool status);

    // BaseDevice interface
public:
    QWidget *GetConfigWidget() override;
    IOItem *GetIOItem(QString ioName);

protected:
    QList<IOItem*> _allIOItems;
    QList<QString> _showItems;

public:
    virtual void SetOtherConfig(QJsonObject other) override;

    // BaseDevice interface
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;
};

#endif // BASEIOMODULE_H
