﻿#include "FormIOModule.h"
#include "ui_FormIOModule.h"
#include <exception>
#include <QException>
#include <QLabel>
#include <QLayout>
#include <QVBoxLayout>

#include <DeviceManager.h>
#include <IOItem.h>

FormIOModule::FormIOModule(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormIOModule),
    _labelList(new QList<QLabel*>)
{
    ui->setupUi(this);
    _timerId=startTimer(1000);
}

FormIOModule::~FormIOModule()
{
    killTimer(_timerId);
    delete ui;
}

void FormIOModule::SetIOModule(BaseIOModule *ioModule)
{
    _ioModule=ioModule;
    _allIODevice=ioModule->GetAllIOItems();
    Init();
}

void FormIOModule::Init()
{
    _model=new QStandardItemModel();
    ui->tableView->setModel(_model);
    _model->setColumnCount(5);
    _model->setHorizontalHeaderLabels(QStringList()<<"name"<<"addr"<<"device type"<<"value"<<"control");

    for(int i=0;i<_allIODevice.count();i++)
    {
        IOItem* dev=_allIODevice.at(i);

        _model->setItem(i,0,new QStandardItem(dev->GetName()));
        _model->setItem(i,1,new QStandardItem(QString("%1").arg(dev->Addr())));
        _model->setItem(i,2,new QStandardItem(QString("%1").arg(dev->DeviceTypeString())));

        QLabel* label = new QLabel(this);
        label->setFixedSize(20, 20);
        label->setAlignment(Qt::AlignCenter);
        label->setStyleSheet(GetColorStyleSheet("red"));
        QHBoxLayout *layout=new QHBoxLayout(this);
        layout->setSpacing(0);
        layout->setMargin(0);
        layout->addWidget(label);
        QWidget *wd=new QWidget(this);
        wd->setLayout(layout);
        if(dev->DeviceType()==IODeviceType::Input)
        {
            wd->setEnabled(false);
        }
        ui->tableView->setIndexWidget(_model->index(i,3),wd);
        _labelList->append(label);
    }

    // 必须要设置此项，否则样式表的hover无法生效
    ui->tableView->setMouseTracking(true);
    // 构造函数中传入按钮列表即可添加任意个按钮
    m_btnDelegate = new QMyTableViewBtnDelegate(QStringList()<<"open"<<"close", this);
    // 为指定列设置代理
    ui->tableView->setItemDelegateForColumn(4, m_btnDelegate);
    // 连接信号槽，根据需要添加
    connect(m_btnDelegate, &QMyTableViewBtnDelegate::editData, this, &FormIOModule::Open);
    connect(m_btnDelegate, &QMyTableViewBtnDelegate::deleteData, this, &FormIOModule::Close);

    QList<int> widths={150,100,100,100,200};
    for(int i=0;i<widths.count();i++)
    {
        ui->tableView->setColumnWidth(i,widths[i]);
    }

    for(int i=0;i<_allIODevice.count();i++)
    {
        ui->tableView->setRowHeight(i,40);
    }
}

QString FormIOModule::GetColorStyleSheet(QString color)
{
    return QString("border-radius: 10px; border: 1px solid black; background: %1").arg(color);
}

void FormIOModule::Open(const QModelIndex &index)
{
    _ioModule->WriteStatus(_allIODevice.at(index.row())->GetName(),true);
}

void FormIOModule::Close(const QModelIndex &index)
{
     _ioModule->WriteStatus(_allIODevice.at(index.row())->GetName(),false);
}

void FormIOModule::on_btnStartDetect_clicked()
{
    for(int i=0;i<_allIODevice.count();i++)
    {
         bool ret=_allIODevice.at(i)->GetStatus();
         QString color=ret?"green":"red";
        _labelList->at(i)->setStyleSheet(GetColorStyleSheet(color));
    }
}

void FormIOModule::timerEvent(QTimerEvent *event)
{
    on_btnStartDetect_clicked();
}
