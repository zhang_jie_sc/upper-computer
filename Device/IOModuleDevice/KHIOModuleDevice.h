﻿#ifndef KHIOMODULEDEVICE_H
#define KHIOMODULEDEVICE_H

#include <QObject>
#include <QJsonObject>
#include <QList>

#include "IOItem.h"
#include "BaseIOModule.h"

class KHIOModuleDevice : public BaseIOModule
{
    Q_OBJECT

public:
    Q_INVOKABLE KHIOModuleDevice(QObject *parent = nullptr);

signals:

public slots:

protected:
    BaseModbus* _modbus=nullptr;
    bool _inReading=false;

    // BaseIOModule interface
public:
    bool ReadStatus(QString ioName, bool &status) override;
    bool WriteStatus(QString ioName, bool status) override;
    void SetCommunication(QList<BaseCommunication *> communication) override;
    void GetConnectStateCore(DeviceStateItems &items) override;

private:
    bool FindStatus(QString dataType,bool &status);
};
#endif // KHIOMODULEDEVICE_H
