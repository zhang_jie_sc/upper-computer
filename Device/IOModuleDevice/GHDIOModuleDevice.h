﻿#ifndef GHDIOMODULEDEVICE_H
#define GHDIOMODULEDEVICE_H

#include <QObject>
#include "IOItem.h"
#include "KHIOModuleDevice.h"

class GHDIOModuleDevice : public KHIOModuleDevice
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit GHDIOModuleDevice(QObject *parent = nullptr);

signals:

public slots:

    // BaseDevice interface
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;

    // BaseIOModule interface
public:
    virtual bool WriteStatus(QString ioName, bool status) override;
};

#endif // GHDIOMODULEDEVICE_H
