﻿#ifndef IODEVICE_H
#define IODEVICE_H
#include "Head.h"
#include <QObject>
#include <QJsonObject>

#include <QTimer>
#include "BaseModbus.h"
#include "BaseIOModule.h"

class BaseIOModule;

enum IODeviceType
{
    Input,
    Output
};

class IOItem:public QObject
{
    Q_OBJECT
public:
    explicit IOItem(int addr,IODeviceType deviceType,BaseIOModule *ioModule);

    void SetName(QString name);
    QString GetName();

    int Addr();
    QString DeviceTypeString();
    IODeviceType DeviceType();

     bool SetStatus(bool status) ;
     bool SetStatusDelayReset(bool status, int delaySwitchSecond);

     bool GetStatus() ;

protected:

private:
    QString _name;
    bool _status=false;
    int _addr;
    IODeviceType _deviceType;
    BaseIOModule *_ioModule;

    int _interMs=-1;
signals:

public slots:
};

#endif // IODEVICE_H
