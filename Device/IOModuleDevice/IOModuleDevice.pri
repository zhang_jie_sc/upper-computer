INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BaseIOModule.h \
    $$PWD/IOItem.h \
    $$PWD/FormIOModule.h \
    $$PWD/KHIOModuleDevice.h \
    $$PWD/GHDIOModuleDevice.h

SOURCES += \
    $$PWD/BaseIOModule.cpp \
    $$PWD/IOItem.cpp \
    $$PWD/FormIOModule.cpp \
    $$PWD/KHIOModuleDevice.cpp \
    $$PWD/GHDIOModuleDevice.cpp

FORMS += \
    $$PWD/FormIOModule.ui
