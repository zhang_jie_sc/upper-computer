﻿#include "DeviceHelper.h"
#include "DeviceManager.h"

namespace {
    QTableWidget *deviceData = NULL;    //显示表格数据
}

DeviceHelper::DeviceHelper()
{

}

void DeviceHelper::SetTableWidget(QTableWidget *tableData)
{
    deviceData=tableData;
    initTableView(deviceData, 30,true,false,true);
}

void DeviceHelper::initTableView(QTableView *tableView, int rowHeight, bool headVisible, bool edit, bool stretchLast)
{
    //设置弱属性用于应用qss特殊样式
    tableView->setProperty("model", true);
    //取消自动换行
    tableView->setWordWrap(false);
    //超出文本不显示省略号
    tableView->setTextElideMode(Qt::ElideNone);
    //奇数偶数行颜色交替
    tableView->setAlternatingRowColors(false);
    //垂直表头是否可见
    tableView->verticalHeader()->setVisible(headVisible);
    //选中一行表头是否加粗
    tableView->horizontalHeader()->setHighlightSections(false);
    //最后一行拉伸填充
    tableView->horizontalHeader()->setStretchLastSection(stretchLast);
    //行标题最小宽度尺寸
    tableView->horizontalHeader()->setMinimumSectionSize(0);
    //行标题最小高度,等同于和默认行高一致
//    tableView->horizontalHeader()->setFixedHeight(rowHeight);
//    //默认行高
//    tableView->verticalHeader()->setDefaultSectionSize(rowHeight);
    //选中时一行整体选中
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    //只允许选择单个
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);

    //表头不可单击
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
    tableView->horizontalHeader()->setSectionsClickable(false);
#else
    tableView->horizontalHeader()->setClickable(false);
#endif

    //鼠标按下即进入编辑模式
    if (edit) {
        tableView->setEditTriggers(QAbstractItemView::CurrentChanged | QAbstractItemView::DoubleClicked);
    } else {
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
}


void DeviceHelper::initDeviceData()
{
    if (!deviceData) {
        return;
    }

    QList<QString> columnNames;
    columnNames<<"设备名"<<"地址"<<"数据类型"<<"实时数据"<<"连接状态";
    QList<int> columnWidths;
    columnWidths << 150 << 200 << 150 << 150 << 150 ;

    //设置列数列宽标题文本集合等
    int columnCount = columnNames.count();
    deviceData->setColumnCount(columnCount);
    deviceData->setHorizontalHeaderLabels(columnNames);
    for (int i = 0; i < columnCount; i++)
    {
        deviceData->setColumnWidth(i, columnWidths.at(i));
        if(i==3)
        {
            deviceData->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Stretch);
        }
        else
        {
            deviceData->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
        }

    }
    QList<BaseDevice*> devices=DeviceManager::Instance().GetItems<BaseDevice>();

    DeviceStateItems statusList;
    for(int i=0;i<devices.count();i++)
    {
        statusList.append(devices.at(i)->GetDeviceStatusConfig());
    }
    deviceData->setRowCount(statusList.count());
    for(int i=0;i<statusList.count();i++)
    {
        DeviceStateItem item=statusList.at(i);
        QStringList data;
        data<<item.Name<<item.Address<<item.DataType<<item.Data<<"离线";
        addData(data);
    }
}

void DeviceHelper::addData(const QStringList &data)
{
    if (!deviceData) {
        return;
    }

    //需要居中的列索引集合
    QList<int> centerColumn;
    centerColumn << 0 << 1 << 2 << 3 << 4 << 5;

    //这里行数还必须是静态变量因为会多次调用
    static int row = -1;
    row++;
    for (int i = 0; i < data.count(); ++i) {
        QTableWidgetItem *item = new QTableWidgetItem(data.at(i));
        if (centerColumn.contains(i)) {
            item->setTextAlignment(Qt::AlignCenter);
        }
        //加粗+不同颜色显示对应的数值和状态
        deviceData->setItem(row, i, item);
    }
}

void DeviceHelper::SetStatusStatus(DeviceStateItems statusList)
{
    for(int i=0;i<statusList.count();i++)
    {
        DeviceStateItem item=statusList.at(i);
        int index=GetItemIndex(item.Name,item.DataType);
        if(index!=-1)
        {
            deviceData->item(index,1)->setData(Qt::DisplayRole,item.Address);
            deviceData->item(index,3)->setData(Qt::DisplayRole,item.Data);
            deviceData->item(index,4)->setData(Qt::DisplayRole,item.IsConnected?"在线":"离线");
        }
    }
}

int DeviceHelper::GetItemIndex(QString deviceName,QString dataType)
{
    for(int i=0;i<deviceData->rowCount();i++)
    {
        if(deviceName== deviceData->item(i,0)->data(Qt::DisplayRole).toString()&&
                dataType== deviceData->item(i,2)->data(Qt::DisplayRole).toString())
        {
            return i;
        }
    }
    return -1;
}
