INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/BasePretreatment.h \
    $$PWD/DialogPretreament.h \
    $$PWD/PowderPretreatment.h

SOURCES += \
    $$PWD/BasePretreatment.cpp \
    $$PWD/DialogPretreament.cpp \
    $$PWD/PowderPretreatment.cpp

FORMS += \
    $$PWD/DialogPretreament.ui
