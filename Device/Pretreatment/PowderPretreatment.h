﻿#ifndef POWDERPRETREATMENT_H
#define POWDERPRETREATMENT_H

#include "BasePretreatment.h"
#include "BaseModbus.h"

class PowderPretreatment : public BasePretreatment
{
    Q_OBJECT
public:
    Q_INVOKABLE PowderPretreatment(QObject *parent = nullptr);

signals:

public slots:

// BaseDevice interface
public:
    virtual void SetCommunication(QList<BaseCommunication *> communication) override;
    virtual bool Stop() override;
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;

// BasePretreatment interface
public:
    virtual void Start() override;
    virtual bool Reset() override;
private:
    BaseModbus *_modbus=nullptr;
};

#endif // POWDERPRETREATMENT_H
