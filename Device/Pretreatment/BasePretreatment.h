﻿#pragma execution_character_set("utf-8")
#ifndef BASEPRETREATMENT_H
#define BASEPRETREATMENT_H
#include <QDebug>
#include "BaseDevice.h"
#include "DialogPretreament.h"

enum RunMode
{
    Off=0x00,
    Single=0x01,
    Internal=0x02,
    Continuous=0x03,
    Ready=0x04
};

class BasePretreatment:public BaseDevice
{
    Q_OBJECT
public:
    Q_INVOKABLE BasePretreatment(QObject *parent=nullptr);

    //启动测量
    virtual void Start();
    //设备停止
    virtual bool Stop() override;
    //设备复位
    virtual bool Reset() override;

    // BaseItem interface
public:
    virtual QWidget *GetConfigWidget() override final;

    // BaseDevice interface
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;
};

#endif // BASEPRETREATMENT_H
