﻿#ifndef MPF_DATA_TYPE_H
#define MPF_DATA_TYPE_H

//定义使用的宏
//#define AXIS_RANGE_MAX            34000  //平移台各轴的最大运动范围
//#define AXIS_RANGE_MIN            0      //平移台各轴的最小运动范围
#define AXIS_RANGE_MAX            16     //平移台各轴的最大运动范围
#define AXIS_RANGE_MIN            -16      //平移台各轴的最小运动范围
#define SC300_MMtoStep            1800   //mm转换为脉冲数量

#define MAX_WAIT_TIME    20000             //串口等待数据到达最大等待时间

#define GS_MMTOSTEP    1600             //gs导轨脉冲与mm的转换


//测试的运动轴
typedef enum {
    AXIS_MIN = -1,
    AXIS_X,
    AXIS_Y,
    AXIS_Z,
    AXIS_MAX,
    AXIS_NONE,
}AXIS_ENUM;

//数据类型枚举
typedef enum {
    DATA_MIN = -1,
    DATA_POS,                   //位置参数
    DATA_VEL,                   //速度参数
    DATA_MAX
}DATA_TYPE_ENUM;

//速度数据类型枚举
typedef enum {
    VEL_MIN = -1,
    VEL_TOPSPEED,              //顶端速度
    VEL_INITSPEED,             //初速度，减速读
    VEL_ACCSPEED,              //加速度
    VEL_MAX
}VEL_TYPE_ENUM;

//定义界面单位类型枚举
typedef enum {
    UNIT_TYPE_MIN = -1,
    UNIT_TYPE_STEP,     //步数
    UNIT_TYPE_MM,       //毫米单位
}UNIT_TYPE_ENUM;

typedef struct ControlParaTag {
    char charFunc[256];
    char commName[20];
    AXIS_ENUM eAxis;
    VEL_TYPE_ENUM eVel;
    int iUnit;          //0：脉冲；1:毫米; 2:弧度
    double dPrecision;  //脉冲到毫米或者弧度的转换精度
    double dCountValue;  //指令计数设置值
    double dVel;   //相对移动距离
    double dRelativeMove;   //相对移动距离
    double dMove;           //绝对移动距离
    double dPos;            //当前位置
}ControlPara;

Q_DECLARE_METATYPE(ControlPara);

#endif // MPF_DATA_TYPE_H
