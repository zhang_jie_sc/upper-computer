﻿#ifndef GSMOTORCONTROLLER_H
#define GSMOTORCONTROLLER_H
#include "BaseMotorController.h"
#include "BaseSendReply.h"

//中文表格编程指令系统 就是这个命名也没有具体厂家，有点奇怪
class GSMotorController : public BaseMotorController
{
    Q_OBJECT
public:
    enum MoveType
    {
        Relative,
        Absolute
    };
    Q_ENUM(MoveType)

    Q_INVOKABLE explicit GSMotorController(QObject *parent=nullptr);

    // BaseDevice interface
public:
    virtual void SetCommunication(QList<BaseCommunication *> communication) override;

    // BaseMotorController interface
public:
    virtual void SetCurrentAxisType(AxisType axisType) override;
    virtual int SetVelocity(VEL_TYPE_ENUM eVelType, double dVelocity) override;
    virtual int GetVelocity(VEL_TYPE_ENUM eVelType, double &dVelocity) override;
    virtual int RelativeMove(double dRelativeMove) override;
    virtual int AbsoluteMove(double dMove) override;
    virtual int SetCurrentPositionToZero() override;
    virtual int GetPosition(double &dPosition) override;
    virtual bool GetMotorStatus() override;
    bool SetLimit(DirectionType limitType, bool enable);
    bool GetLimit(DirectionType limitType, bool &enable);
    virtual bool Stop() override;

private:
    BaseSendReply *_sender=nullptr;
    QMap<AxisType,uchar> _velocityMap;
    QMap<AxisType,int> _axisMotorMap;
    bool writeDataToRegister(uchar addr, double value);
    bool readDataFromRegister(uchar addr, QByteArray &readData);
    int setMotorPara(MoveType bAbsolutionMove, AxisType eAxis, double dMoveValue);
    bool writeMotorCmd(uchar cmd, QByteArray &readData);
    void WaitForMotorStop();
    QByteArray CreateCommand(uchar funcCode, uchar addr,QList<uchar> value=QList<uchar>());
    QByteArray CreateCommand(uchar funcCode, QList<uchar> value=QList<uchar>());
    uchar GetCurrentVelAddr();
    uchar GetCurrentAxisAddr();
};

#endif // GSMOTORCONTROLLER_H
