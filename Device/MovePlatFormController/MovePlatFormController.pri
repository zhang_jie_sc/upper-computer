INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/mpf_data_type.h \
    $$PWD/CL2CRSDriveControl.h \
    $$PWD/mpf_data_type.h \
    $$PWD/dialogmoveplatform.h \
    $$PWD/BaseMotorController.h \
    $$PWD/GSMotorCommand.h

SOURCES += \
    $$PWD/CL2CRSDriveControl.cpp \
    $$PWD/dialogmoveplatform.cpp \
    $$PWD/BaseMotorController.cpp \
    $$PWD/GSMotorCommand.cpp

FORMS += \
    $$PWD/dialogmoveplatform.ui
