﻿#ifndef MOVEPLATFORM_H
#define MOVEPLATFORM_H

#include <QDebug>
#include <QObject>
#include <QSettings>
#include "mpf_data_type.h"
#include "BaseDevice.h"

class StateItem
{
public:
    StateItem(QString name,QString annotation="")
    {
        Name=name;
        State="";
        Annotation=annotation;
    }

    QString Name;
    QString State;
    QString Annotation;
};

enum PolartyType
{
    Open,
    Close
};

class MotorIOItem
{
public:
    MotorIOItem() {}

    QString Name;

    uint16_t FunctionType;

    PolartyType PloartyType;
};

//定义设置运行速度、设置运行轴、相对移动、绝对运动、位置获取、停止运行、重置零位、设置限位使能、获取限位使能状态
class BaseMotorController: public BaseDevice
{
    Q_OBJECT

public:
    //测试的运动轴
    enum AxisType{
        AXIS_MIN=0,
        AXIS_X=1,
        AXIS_Y=2,
        AXIS_Z=3
    };
    Q_ENUM(AxisType)

    enum DirectionType{
        Back=1,
        Forward=2
    };
    Q_ENUM(DirectionType)

    //轴运动方向
    enum AxisDirectionType{
        AxisForward=1,//正向
        AxisReverse=2//反向
    };
    Q_ENUM(AxisDirectionType)

    enum LimitType
    {
       Limit,
       Zero
    };
    Q_ENUM(LimitType)

    Q_INVOKABLE BaseMotorController(QObject *parent = 0);
    ~BaseMotorController();

    virtual void SetCurrentAxisType(AxisType axisType);
    virtual void SetMotorEnable(bool enable);

    virtual int SetVelocity(VEL_TYPE_ENUM eVelType, double dVelocity);  //设置轴的速度

    virtual int RelativeMove(double dRelativeMove);   //设置轴的相对移动位置
    virtual int AbsoluteMove(double dMove);           //设置轴的绝对移动位置
    virtual int SetCurrentPositionToZero();              //设置轴的位置归零,感应限位0
    virtual int GetPosition(double &dPosition);          //获取当前轴的位置

    virtual void SetLimit(QList<MotorIOItem> items);

    virtual void ReturnToZero(DirectionType direction,LimitType limit);

    virtual void SetAxisDriection(AxisDirectionType axisType);

    //设置峰值电流
    virtual void SetMaxElectricity(int electricity);

    virtual QList<StateItem> GetStateItems();

    virtual bool Stop() override;                           //停止运动

    virtual bool SaveParam();

    virtual bool Reset() override;

    // BaseDevice interface
public:
    virtual QWidget *GetConfigWidget() override final;
    virtual QSharedPointer<QWidget> GetConfigWidgetZZZ() override final;

     QSettings *Set=nullptr;

protected:
    QWidget *_configW=nullptr;
    bool _negitive=false;
    bool _positive=false;
    AxisType _axisType=AxisType::AXIS_X;
    bool _motorStatus=false;

};

#endif // MOVEPLATFORM_H
