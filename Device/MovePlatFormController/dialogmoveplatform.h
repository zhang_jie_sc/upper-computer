﻿#ifndef MOVEPLATFORMSYS_H
#define MOVEPLATFORMSYS_H

#include <QWidget>
#include <QGridLayout>
#include <QDialog>
#include <QTimer>
#include <QComboBox>
#include <QTableWidget>
#include <QSettings>

#include "mpf_data_type.h"
#include "BaseMotorController.h"
#include "CL2CRSDriveControl.h"


#define MAX_CURVE_NUM  100

namespace Ui {
    class DialogMovePlatform;
}

class DialogMovePlatform : public QDialog
{
    Q_OBJECT

public:
    explicit DialogMovePlatform(QWidget *parent=nullptr);
    ~DialogMovePlatform();

    void SetDevice(BaseMotorController* platform);

    virtual void timerEvent(QTimerEvent *event) override;

    void closeEvent(QCloseEvent *event) override;

private slots:

    void on_btnMotorOpen_clicked();

    void on_btnMotorClose_clicked();

    void on_IDC_Btn_RunVel_Set_clicked();

    void on_IDC_Btn_AbsoluteMove_clicked();

    void on_IDC_Btn_ResetZeroCor_clicked();

    void on_IDC_Btn_StopMove_clicked();

    void on_IDC_Btn_RelativeMoveForward_clicked();

    void on_IDC_Btn_RelativeMoveReverse_clicked();

    void on_radAxisX_clicked(bool checked);

    void on_radAxisY_clicked(bool checked);

    void on_radAxisZ_clicked(bool checked);

    void on_btnSetLimit_clicked();

    void on_btnReturnToZero_clicked();

    void on_btnSetCurPosZero_clicked();

    void on_btnSaveParam_clicked();

    void on_btnResetFactory_clicked();

    void on_IDC_Btn_SetMaxElectricity_clicked();

    void on_btnSetAxisDirection_clicked();

    void on_btnSaveFocus_clicked();

    void on_btnLoadFocus_clicked();

private:
    Ui::DialogMovePlatform *ui;

    BaseMotorController *_movePlatform=nullptr;

    void InitTable();
    void TableWidgetAddComboBox(QTableWidget *tableWidget, int x, int y, QStringList items, int comboBoxIdx);

    QList<MotorIOItem> GetMotorIOItems();

    bool _isSaved=false;
    void SaveParam();
    QSettings * GetQSetings();
};

#endif // MOVEPLATFORMSYS_H
