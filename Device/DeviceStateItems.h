﻿#ifndef DEVICESTATUSLIST_H
#define DEVICESTATUSLIST_H

#include <QObject>
#include <QList>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonArray>
#include "DeviceStateItem.h"

class DeviceStateItems : public QList<DeviceStateItem>
{
    QOBJECT_H
public:
    explicit DeviceStateItems();
    QDateTime TimeKey;

    static QJsonObject ConvertToJsonObj(DeviceStateItems items);
signals:

public slots:
};

#endif // DEVICESTATUSLIST_H
