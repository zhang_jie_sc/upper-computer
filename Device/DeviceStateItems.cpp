﻿#include "DeviceStateItems.h"

DeviceStateItems::DeviceStateItems()
{

}

QJsonObject DeviceStateItems::ConvertToJsonObj(DeviceStateItems items)
{
    QJsonArray arr;
    for(DeviceStateItem item:items)
    {
        QJsonObject obj={
            {"Name",item.Name},
            {"DataType",item.DataType},
            {"Value",item.Data}
        };
        arr.append(obj);
    }
    QJsonObject ret={
        {"Items",arr}
    };
    return ret;
}
