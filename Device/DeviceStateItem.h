﻿#ifndef DEVICESTATUSITEM_H
#define DEVICESTATUSITEM_H
#include "Head.h"
#include <QObject>

struct DeviceStateItem
{
public:
    DeviceStateItem();
    DeviceStateItem(QString name,QString address);
    QString Name;
    QString Address;
    QString DataType;
    QString Data;
    QString Error;
    bool IsConnected;

    static QStringList GetHeaders();
    QMap<QString,QVariant> GetMap();
};

#endif // DEVICESTATUSITEM_H
