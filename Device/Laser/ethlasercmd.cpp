﻿#pragma execution_character_set("utf-8")
#include <QDebug>
#include "ethlasercmd.h"
#include "CrcUtils.h"
#include "bitutils.h"

EthLaserCmd::EthLaserCmd(QObject *parent) : QObject(parent)
{

}

QByteArray EthLaserCmd::GetData(QByteArray fullFrameData)
{
    char header=fullFrameData.at(0);
    int payloadLength=static_cast<int>(fullFrameData.at(1));
    QByteArray ret=fullFrameData.mid(3,payloadLength-1);
    return ret;
}

QJsonObject EthLaserCmd::GetSystemStatus(QByteArray data)
{
    QList<QString> statusKey=
        {
            "使能","故障状态","预热状态","前级电压[V]",
            "通信状态","电流设定[A]","电流反馈[A]","电源风扇状态",
            "激光器风扇状态","光闸","放电次数","触发模式",
            "内触发频率","A温度","B温度","C温度",
            "D温度","环境温度"
        };
    QJsonObject ret;
    int index=0;
    for(int i=0;i<statusKey.count();i++)
    {
        QString key=statusKey.at(i);
        if(key=="光闸"||key=="放电次数"||key=="触发模式")
        {
            QByteArray tempArr=data.mid(index,4);
            ret[key]= BitUtils::ArrayToInt(tempArr);
            index+=4;
        }
        else if(key=="内触发频率")
        {
            ret[key]= BitUtils::ArrayToFloat(data.mid(index,4));
            index+=4;
        }
        else if(key.contains("温度"))
        {
            ret[key]= BitUtils::ArrayToFloat(data.mid(index,4));
            index+=4;
        }
        else
        {
            char myChar=data.mid(index,1).at(0);
            ret[key]=QString::number(static_cast<unsigned char>(myChar), 10);
//            qDebug()<<__FUNCTION__<<__LINE__<<key<<ret[key]<<data.mid(index,1);
            index+=1;
        }
    }

    return ret;
}
