﻿#ifndef ETHLASERCMD_H
#define ETHLASERCMD_H

#include <QObject>
#include <QByteArray>
#include <QJsonObject>

class EthLaserCmd : public QObject
{
    Q_OBJECT
public:
    explicit EthLaserCmd(QObject *parent = nullptr);

    static QByteArray GetData(QByteArray fullFrameData);

    static QJsonObject GetSystemStatus(QByteArray data);
signals:

public slots:
};

#endif // ETHLASERCMD_H
