﻿#include "AirCooledLaser.h"
#include "quihelperdata.h"

AirCooledLaser::AirCooledLaser(QObject *parent):BaseLaser(parent)
{

}

AirCooledLaser::~AirCooledLaser()
{

}

void AirCooledLaser::Open()
{
    QByteArray sendBuf = QByteArray::fromHex("aa0011cc33c33c");
    _sendReply->SendNoReply(sendBuf);
}

void AirCooledLaser::Close()
{
    QByteArray cmd=QByteArray::fromHex("aa0010cc33c33c");
    _sendReply->SendNoReply(cmd);
}

bool AirCooledLaser::GetStatus(QJsonObject &obj)
{
     QByteArray cmd=QByteArray::fromHex("aa00bbcc33c33c");
     QByteArray rec;
     _sendReply->SendReply(cmd,rec);
     //AA 00 BB 00 00 00 7B 0C 46 01 01 01 CC 33 C3 3C
     //aa 回传地址 0xbb 故障状态 工作状态 计数器最高位 计数器2位 计数器3位 计数器最低位 联锁状态 温度状态 外时钟状态 cc 33 c3 3c
     bool ok;
     if(rec.count()>12)
     {
         QJsonObject ret{
             {"故障状态[0x08前级欠压 0x80过流]",rec.at(3)},
             {"工作状态[0停机 1工作]",rec.at(4)},
             {"放电次数",rec.mid(5,4).toHex().toInt(&ok,16)},
             {"连锁状态[0连锁 1正常]",rec.at(9)},
             {"温度状态[0温度过高 1正常]",rec.at(10)},
             {"外时钟状态[0过快 1正常]",rec.at(11)}
         };
         obj=ret;
     }
     else
     {
         obj={{"数据错误","err"}};
     }

     return true;
}

void AirCooledLaser::SetCommunication(QList<BaseCommunication *> communication)
{
    BaseDevice::SetCommunication(communication);
    if(communication.empty())
    {
        throw QString("激光器 通讯实体 为空");
    }
    _sendReply=dynamic_cast<BaseSendReply*>(communication.first());
}

void AirCooledLaser::SetFrequence(int freq)
{
    char arr[9] = {0xAa,0x00,0x66,0x00,freq,0xCC,0x33,0xC3,0x3C};
    _sendReply->SendNoReply1(arr,sizeof(arr));
}

void AirCooledLaser::SetEnergy(int level)
{
    char arr[]={0xaa,0x00,0x22,0x00,level,0xcc,0x33,0xc3,0x3c};
    _sendReply->SendNoReply1(arr,sizeof(arr));
}

int AirCooledLaser::SwitchShutter(bool bopen)
{
    char openarr[9] = {0xAa,0x00,0x13,0xCC,0x33,0xC3,0x3C};
    char closearr[9] = {0xAa,0x00,0x12,0xCC,0x33,0xC3,0x3C};
    if(bopen) {
        _sendReply->SendNoReply1(openarr,sizeof(openarr));
    } else {
        _sendReply->SendNoReply1(closearr,sizeof(openarr));
    }
    return 0;
}


