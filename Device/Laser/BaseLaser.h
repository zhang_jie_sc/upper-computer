﻿#ifndef BASELASER_H
#define BASELASER_H
#include <BaseDevice.h>

//触发模式
enum LASER_SYNC_MODE
{
    LASER_FLASHLAMP_SYNC_MODE_MIN = -1,
    LASER_FLASHLAMP_SYNC_INTERNAL,
    LASER_FLASHLAMP_SYNC_EXTERNAL,
    LASER_FLASHLAMP_SYNC_MODE_MAX
};

//激光器控制基类，定义测量流程中需要的方法
//激光器需要有触发设置、能量设置、使能设置、频率设置、状态获取几个功能，状态获取是以json格式数据返回的
class BaseLaser:public BaseDevice
{
    Q_OBJECT
public:
    Q_INVOKABLE BaseLaser(QObject *parent=nullptr);

    // BaseDevice interface
public:
    QWidget *GetConfigWidget() override;

    // BaseLaser Open Close 触发设置 能量 频率设置
public:
    virtual void Open();
    virtual void Close();
    //设置触发模式
    virtual void SetFlashlampSyncMode(LASER_SYNC_MODE mode);
    //设置频率
    virtual void SetFrequence(int freq);
    //设置能量等级
    virtual void SetEnergy(int level);

    virtual bool GetStatus(QJsonObject &obj);

    QList<int> GetAdviceFrequency();


    // BaseDevice interface
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;

    virtual bool Stop() override;
};

#endif // BASELASER_H
