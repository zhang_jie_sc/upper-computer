﻿#ifndef LASERETHDRIVER_H
#define LASERETHDRIVER_H
#include "Head.h"
#include <QObject>
#include <QTcpSocket>
#include <QTimer>

#include "BaseLaser.h"
#define BUF_MAX    128

//参考N3004-3白电源通讯协议.pdf
class CNILaser : public BaseLaser
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit CNILaser(QObject *parent=nullptr);
    ~CNILaser();

    static QByteArray CreateControlCmd(int funCode, int enable);

    // BaseLaser interface
public:
    void Open() override;
    void Close() override;
    void SetFlashlampSyncMode(LASER_SYNC_MODE mode) override;
    void SetFrequence(int freq) override;
    void SetEnergy(int level) override;
    bool GetStatus(QJsonObject &obj) override;

    // BaseDevice interface
public:
    void SetCommunication(QList<BaseCommunication *> communication) override;

private:
    void waitMoment(int msec = 1000);
    bool writeMsg(QByteArray sendbuf, QByteArray &recvData);

    int LaserShutterSwitch(bool bopen);           //光闸开关
    int LaserEnableSwitch(bool bopen);            //使能开关

private:
    BaseSendReply *_sendReply=nullptr;

private slots:

    // BaseDevice interface
protected:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;
};

#endif // LASERETHDRIVER_H
