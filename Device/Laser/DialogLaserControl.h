﻿#ifndef DIALOGLASERCONTROL_H
#define DIALOGLASERCONTROL_H

#include <QDialog>

#include "BaseLaser.h"

namespace Ui {
class DialogLaserControl;
}

class DialogLaserControl : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLaserControl(QWidget *parent = 0);
    ~DialogLaserControl();
    void SetLaser(BaseLaser *laser);

    void  showWarning(QString warningText);
 private slots:

     void on_IDC_Btn_StateQuery_clicked();

     void on_Btn_frequence_Set_clicked();

     void on_cmbTrigType_currentIndexChanged(int index);

     void on_cmbDeviceEnable_currentIndexChanged(int index);

     void on_cmbEnergy_currentIndexChanged(int index);

     void on_btnQueryStatus_clicked();

     void on_btnFrequence_clicked();

private:
     Ui::DialogLaserControl *ui;

     BaseLaser *_laserControl = NULL;
};

#endif // DIALOGLASERCONTROL_H
