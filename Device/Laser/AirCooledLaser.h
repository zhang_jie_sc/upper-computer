﻿#ifndef AIRCOOLEDLASER_H
#define AIRCOOLEDLASER_H

#include <QObject>
#include "BaseLaser.h"

class AirCooledLaser:public BaseLaser
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit AirCooledLaser(QObject *parent=nullptr);
    ~AirCooledLaser();

    // BaseLaser interface
public:
    void Open() override;
    void Close() override;
    bool GetStatus(QJsonObject &obj) override;

    // BaseDevice interface
public:
    void SetCommunication(QList<BaseCommunication *> communication) override;

private:
    BaseSendReply *_sendReply=nullptr;

private slots:

    // BaseLaser interface
public:
    virtual void SetFrequence(int freq) override;
    virtual void SetEnergy(int level) override;

private:
     int SwitchShutter(bool bopen);   //开关光闸
};

#endif // AIRCOOLEDLASER_H
