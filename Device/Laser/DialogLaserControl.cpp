﻿#include "DialogLaserControl.h"
#include "ui_DialogLaserControl.h"

#include <QMessageBox>
#include <QDebug>
#include <QJsonDocument>

DialogLaserControl::DialogLaserControl(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogLaserControl)
{
    ui->setupUi(this);
    ui->label_2->hide();
    ui->cmbTrigType->hide();
}

DialogLaserControl::~DialogLaserControl()
{
    delete ui;
}

void DialogLaserControl::SetLaser(BaseLaser *laser)
{
    _laserControl=laser;
    QList<int> freq=_laserControl->GetAdviceFrequency();
    ui->cmbFrequency->clear();
    for(int i=0;i<freq.count();i++)
    {
        ui->cmbFrequency->addItem(QString::number(freq.at(i)),freq.at(i));
    }
}

void DialogLaserControl::showWarning(QString warningText)
{
    QMessageBox *msgBox;
    msgBox = new QMessageBox();
    msgBox->setText(warningText);
    msgBox->setGeometry(600,300,60,30);
    msgBox->show();
}

void DialogLaserControl::on_IDC_Btn_StateQuery_clicked()
{
    int iErrorCode = 0;
    QString status="";
    QJsonObject statusObj;
    iErrorCode = _laserControl->GetStatus(statusObj);
    ui->txtLog->setText(status);
    if(0 != iErrorCode)
    {
       qDebug()<<__FUNCTION__<<__LINE__<<QString("query laser status error");
    }
}

void DialogLaserControl::on_Btn_frequence_Set_clicked()
{

}

void DialogLaserControl::on_cmbTrigType_currentIndexChanged(int index)
{
    LASER_SYNC_MODE mode=index==0?LASER_SYNC_MODE::LASER_FLASHLAMP_SYNC_INTERNAL:LASER_SYNC_MODE::LASER_FLASHLAMP_SYNC_EXTERNAL;
    _laserControl->SetFlashlampSyncMode(mode);
}

void DialogLaserControl::on_cmbDeviceEnable_currentIndexChanged(int index)
{
    if(0==index)
    {
        _laserControl->Close();
    }
    else
    {
        _laserControl->Open();
    }
}

void DialogLaserControl::on_cmbEnergy_currentIndexChanged(int index)
{
    qDebug()<<__FUNCTION__<<__LINE__<<index;
    _laserControl->SetEnergy(index);
}

void DialogLaserControl::on_btnQueryStatus_clicked()
{
    QJsonObject obj;

    if(_laserControl->GetStatus(obj))
    {
        QJsonDocument doc(obj);
        QString status=QString::fromUtf8(doc.toJson(QJsonDocument::Indented));
        ui->txtLog->setText(status);
    }
}

void DialogLaserControl::on_btnFrequence_clicked()
{
    int freq = ui->cmbFrequency->currentText().toInt();
    _laserControl->SetFrequence(freq);
}
