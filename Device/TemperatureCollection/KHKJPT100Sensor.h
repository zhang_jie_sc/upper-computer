﻿#ifndef KHKJPT100SENSOR_H
#define KHKJPT100SENSOR_H

#include <QObject>
#include <QTimer>
#include <QMutex>
#include <QMap>
#include "BaseTemperature.h"
#include "BaseModbus.h"

class KHKJPT100Sensor : public BaseTemperature
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit KHKJPT100Sensor(QObject *parent=nullptr);
    ~KHKJPT100Sensor();

private slots:
    void getSensorValueSlot();

private:
    QTimer m_clocker;
    QList<double> _values;
    BaseModbus *_modbus;

    // BaseDevice interface
public:
    void SetCommunication(QList<BaseCommunication *> communication) override;

    // BaseTemperature interface
public:
    int GetTemperature(QList<double> &values) override;

    // BaseDevice interface
public:
    virtual void GetConnectStateCore(DeviceStateItems &items) override;
};

#endif // KHKJPT100SENSOR_H
