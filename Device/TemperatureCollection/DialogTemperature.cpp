﻿#include "DialogTemperature.h"
#include "ui_DialogTemperature.h"

DialogTemperature::DialogTemperature(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogTemperature)
{
    ui->setupUi(this);
}

DialogTemperature::~DialogTemperature()
{
    delete ui;
}

void DialogTemperature::SetDevice(BaseTemperature *tempture)
{
    _tempture=tempture;
}

void DialogTemperature::on_btnGetData_clicked()
{
    DeviceStateItems lst= _tempture->GetDeviceStateItems();

    // 确保两个 QList 的大小相同
    if (lst.count()>0)
    {
        ui->listWidget->clear();
        for (int i = 0; i < lst.size(); ++i)
        {
            // 创建一个 QListWidgetItem 并设置文本
            QListWidgetItem *item = new QListWidgetItem(lst.at(i).DataType + ": " + lst.at(i).Data);
            // 将 QListWidgetItem 添加到 QListWidget 中
            ui->listWidget->addItem(item);
        }
    }
}
