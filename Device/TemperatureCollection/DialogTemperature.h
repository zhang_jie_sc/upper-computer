﻿#ifndef DIALOGTEMPERATURE_H
#define DIALOGTEMPERATURE_H

#include <QDialog>
#include "BaseTemperature.h"

namespace Ui {
class DialogTemperature;
}

class DialogTemperature : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTemperature(QWidget *parent = 0);
    ~DialogTemperature();
    void SetDevice(BaseTemperature *tempture);

private slots:
    void on_btnGetData_clicked();

private:
    Ui::DialogTemperature *ui;
    BaseTemperature* _tempture;
};

#endif // DIALOGTEMPERATURE_H
