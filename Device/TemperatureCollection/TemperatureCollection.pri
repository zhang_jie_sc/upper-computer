INCLUDEPATH += $$PWD

HEADERS += \ 
    $$PWD/BaseTemperature.h \
    $$PWD/DialogTemperature.h \
    $$PWD/KHKJPT100Sensor.h

SOURCES += \
    $$PWD/BaseTemperature.cpp \
    $$PWD/DialogTemperature.cpp \
    $$PWD/KHKJPT100Sensor.cpp
   

FORMS += \
    $$PWD/DialogTemperature.ui
