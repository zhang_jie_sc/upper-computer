﻿#pragma execution_character_set("utf-8")
#ifndef BASETEMPERATURE_H
#define BASETEMPERATURE_H

#include <QObject>
#include "BaseDevice.h"
#include "BaseModbus.h"

class BaseTemperature : public BaseDevice
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit BaseTemperature(QObject *parent=nullptr);

    virtual int GetTemperature(QList<double> &values);

public slots:

    // BaseDevice interface
public:
    virtual void SetOtherConfig(QJsonObject other) override;
    virtual QWidget *GetConfigWidget() override;

protected:
    QList<QString> _sensorNames;
private:
    QWidget* _configW=nullptr;
};

#endif // BASETEMPERATURE_H
