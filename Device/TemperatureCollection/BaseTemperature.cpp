﻿#include "BaseTemperature.h"
#include <random>
#include <QJsonArray>
#include <QDebug>

#include "DialogTemperature.h"

BaseTemperature::BaseTemperature(QObject *parent):BaseDevice(parent)
{

}

int BaseTemperature::GetTemperature(QList<double> &values)
{
    std::random_device rd;
    std::mt19937 gen(rd()); // 使用Mersenne Twister算法
    std::uniform_int_distribution<int> distribution(300, 530);
    for(QString name:_sensorNames)
    {
        int randomInt = distribution(gen);
        values.append(randomInt/10.0);
    }
    return values.count();
}

void BaseTemperature::SetOtherConfig(QJsonObject other)
{
    BaseDevice::SetOtherConfig(other);
    for(QJsonValue item:other["Items"].toArray())
    {
        _sensorNames.append(item.toObject()["name"].toString());
    }
}

QWidget *BaseTemperature::GetConfigWidget()
{
    qDebug()<<__FUNCTION__<<__LINE__<<_configW;
    if(!_configW)
    {
         qDebug()<<__FUNCTION__<<__LINE__;
        _configW=new DialogTemperature();
    }
    DialogTemperature *w=dynamic_cast<DialogTemperature*>(_configW);
    if(w)
    {
        qDebug()<<__FUNCTION__<<__LINE__;
        w->SetDevice(this);
    }
    return w;
}


