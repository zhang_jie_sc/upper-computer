﻿#ifndef DEVICEHELPER_H
#define DEVICEHELPER_H

#include <QTableWidget>
#include <QTableView>
#include <QHeaderView>
#include "DeviceStateItems.h"

class DeviceHelper
{
public:
    DeviceHelper();

    static void SetTableWidget(QTableWidget *tableData);

    static void initDeviceData();

    static void addData(const QStringList &data);

    static void SetStatusStatus(DeviceStateItems statusList);

    static QTableWidgetItem *GetItem(QString deviceName, QString dataType);
private:
    static void initTableView(QTableView *tableView, int rowHeight, bool headVisible, bool edit, bool stretchLast);
    static int GetItemIndex(QString deviceName, QString dataType);
};

#endif // DEVICEHELPER_H
