﻿#ifndef BASEDEVICEFACTORY_H
#define BASEDEVICEFACTORY_H

#include <QObject>
#include <QMetaObject>
#include <QMap>
#include "BaseDevice.h"
#include "BaseFactory.h"

class BaseDeviceFactory : public BaseFactory
{
public:
    BaseDeviceFactory();
    // BaseFactory interface
public:
    virtual BaseItem* CreateInstance(QString concreteType) override;

signals:

public slots:
};

#endif // IDEVICEFACTORY_H
