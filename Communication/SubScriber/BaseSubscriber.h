﻿#ifndef BASESUBSCRIBER_H
#define BASESUBSCRIBER_H
#include <QDebug>
#include "BaseCommunication.h"

class BaseSubscriber:public BaseCommunication
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit BaseSubscriber(QObject *parent=0);

    virtual bool Publish(QByteArray data);

signals:
    void Receive(QByteArray data);

    // BaseCommunication interface
public:
    virtual QDialog *GetConfigWidget() override;

private:
    QDialog *_configW=nullptr;
};

#endif // BASESUBSCRIBER_H
