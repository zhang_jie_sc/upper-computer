﻿#include "SerialModbus.h"
#include <QJsonObject>
#include <QJsonParseError>
#include <QDebug>
SerialModbus::SerialModbus()
{

}

void SerialModbus::CreateModbusInstance()
{
    QJsonParseError  jsonError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(_communicationParam.toUtf8(), &jsonError);
    if(!jsonDoc.isObject())
    {
        QString err=QString("communicationParam:%1 error!").arg(_communicationParam);
        qCritical()<<__FUNCTION__<<__LINE__<<_communicationParam;
        throw err;
    }
    QJsonObject obj = jsonDoc.object();
    m_ctx=modbus_new_rtu(obj["PortName"].toString().toStdString().c_str(),obj["BaudRate"].toInt(),'N',8,1);
}
