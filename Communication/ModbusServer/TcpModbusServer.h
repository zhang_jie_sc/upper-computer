﻿#ifndef TCPMODBUSSERVER_H
#define TCPMODBUSSERVER_H

#include <QObject>
#include <QModbusTcpServer>
#include "BaseCommunication.h"

class TcpModbusServer : public BaseCommunication
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit TcpModbusServer(QObject *parent = nullptr);

signals:

public slots:

    // BaseCommunication interface
public:
    virtual bool Connect() override;
    virtual bool IsConnected() override;
    virtual void Dispose() override;

    void WriteRegister(QModbusDataUnit::RegisterType registerType,int address, int val);

signals:
    void DataWritten(QModbusDataUnit::RegisterType,int address, int val);

//    bool Write

private slots:
    void onStateChanged(QModbusDevice::State state);
    void onDataWritten(QModbusDataUnit::RegisterType table, int address, int size);

private:
    QModbusTcpServer *_modbusServer=nullptr;

    // BaseItem interface
public:
    virtual void SetOtherConfig(QJsonObject other) override;
};

#endif // TCPMODBUSSERVER_H
