﻿#ifndef TCPMONITOR_H
#define TCPMONITOR_H

#include <QObject>
#include <QThread>
#include <QList>
#include <BaseCommunication.h>

class TCPMonitor : public QThread
{
    Q_OBJECT
public:
    explicit TCPMonitor(QObject *parent = nullptr);

    bool Add(BaseCommunication* comm);

    void run() override;

    void WaitForEnd();
signals:

public slots:

private:
    QList<BaseCommunication*> _communicationList;
    bool _isRunning=true;
};

#endif // TCPMONITOR_H
