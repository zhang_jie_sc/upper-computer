﻿#ifndef BASECOMMUNICATIONFACTORY_H
#define BASECOMMUNICATIONFACTORY_H
#include <QMetaObject>
#include <QMap>
#include "BaseCommunication.h"
#include "BaseFactory.h"

class BaseCommunicationFactory:public BaseFactory
{
public:
    BaseCommunicationFactory();
};

#endif // BASECOMMUNICATIONFACTORY_H
