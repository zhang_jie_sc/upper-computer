﻿#ifndef SERIALPORTSENDREPLYCLIENT_H
#define SERIALPORTSENDREPLYCLIENT_H
#include <QSerialPort>
#include "BaseSendReply.h"

class SerialPortSendReplyClient:public BaseSendReply
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit SerialPortSendReplyClient();

    // BaseCommunication interface
public:
    bool Connect() override;
    void SetCommunicationParam(QString communicationParam) override;
    void Dispose() override;

    // BaseSendReply interface
public:
    bool SendReply(QByteArray sendBuff, QByteArray &receiveBuff) override;
    bool SendNoReply(QByteArray sendBuff) override;

private:
    QSerialPort *_serial=nullptr;
    QList<QByteArray> _recList;

private slots:
    void OnError(QSerialPort::SerialPortError err);
    void OnReadyReady();
};

#endif // SERIALPORTSENDREPLYCLIENT_H
