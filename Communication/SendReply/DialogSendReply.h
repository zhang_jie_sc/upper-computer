﻿#ifndef DIALOGSENDREPLY_H
#define DIALOGSENDREPLY_H

#include <QDialog>
#include "BaseSendReply.h"
class BaseSendReply;

namespace Ui {
class DialogSendReply;
}

class DialogSendReply : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSendReply(QWidget *parent = 0);
    ~DialogSendReply();
    void SetSendReply(BaseSendReply *sendReply);

public  slots:
    void OnDataReceive(QByteArray data);

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void on_btnSendReply_clicked();

    void on_btnCrc_clicked();

    void on_btnSend_clicked();

private:
    Ui::DialogSendReply *ui;
    BaseSendReply* _sendReply=nullptr;
    void AddNewLine(QString line);
    void SetStstus(QString colorName);
};

#endif // DIALOGSENDREPLY_H
