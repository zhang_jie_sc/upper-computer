﻿#ifndef ZMQSENDREPLYSERVER_H
#define ZMQSENDREPLYSERVER_H
#include <QMutex>
#include <QQueue>
#include <QWaitCondition>
#include "BaseSendReply.h"
#include "zmq.h"

class ZmqSendReplyServer:public BaseSendReply
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit ZmqSendReplyServer(QObject* parent=nullptr);

    // BaseCommunication interface
public:
    bool Connect() override;

private:
    //  Socket to talk to clients
    void *_context;
    void *_socket;
    bool _isRunning;
    QWaitCondition  *_waitCondition=nullptr;
    QMutex* _mutex=nullptr;
    QQueue<QByteArray> _sendQueue;

    // BaseCommunication interface
public:
    void Dispose() override;

    // BaseSendReply interface
public:
    virtual bool SendNoReply(QByteArray sendBuff) override;
};

#endif // ZMQSENDREPLYSERVER_H
