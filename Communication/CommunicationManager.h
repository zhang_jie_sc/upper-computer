﻿#pragma execution_character_set("utf-8")
#ifndef COMMUNICATIONMANAGER_H
#define COMMUNICATIONMANAGER_H

#include <QObject>
#include <QList>

#include "tcpmonitor.h"
#include "TcpSendReplyClient.h"
#include "BaseCommunicationFactory.h"
#include "BaseManager.h"

class CommunicationManager : public BaseManager
{
    Q_OBJECT
private:
    Q_INVOKABLE explicit CommunicationManager(QObject *parent = nullptr);
    ~CommunicationManager();
    CommunicationManager(const CommunicationManager&) = delete;
    CommunicationManager& operator=(const CommunicationManager&) = delete;
    int _monitorId;

protected:
    //定时事件
    void timerEvent(QTimerEvent *);

public:
    static CommunicationManager& Instance();

    QList<BaseCommunication*> GetCommunications(QJsonArray names);

    void WaitForEnd();

private:
    bool _enableMonitor=false;
    bool _isMonitoring=false;
signals:

public slots:

    // BaseManager interface
public:

    virtual void InitConfig() override;

    void SetEnableMonitor(bool enableMonitor);
};

#endif // COMMUNICATIONMANAGER_H
