﻿#ifndef ICOMMUNICATIONINIT_H
#define ICOMMUNICATIONINIT_H

#include <QObject>
#include <QJsonObject>
#include <QDialog>
#include "BaseItem.h"

class BaseCommunication:public BaseItem
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit BaseCommunication(QObject *parent = 0);
    virtual void SetCommunicationParam(QString communicationParam);
    virtual QString GetCommunicationParam();
    virtual bool Connect();
    virtual bool IsConnected();
    virtual void Dispose();

    virtual QWidget* GetConfigWidget();
signals:

protected:
    QString _communicationParam;
public slots:

    // BaseItem interface
public:
    virtual void SetConfig(QJsonObject obj) override;
    virtual bool Init() override;

};

#endif // ICOMMUNICATIONINIT_H
