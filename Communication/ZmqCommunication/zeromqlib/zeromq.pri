INCLUDEPATH += $$PWD/include

CONFIG(debug, debug|release):TAIL_FIX=

zeromqLib.path = $$DESTDIR

platPrefix=x86
contains(QMAKE_TARGET.arch, x86_64) {
  platPrefix=x64
 }

message("zeromqLib platform is  " $$platPrefix)

mingw: {
    LIB_DIR =lib_mingw
}

win32-msvc2013: {
    LIB_DIR = vs_2013
}

win32-msvc2015: {
    LIB_DIR = vs_2015
}

LIBS += -L$$PWD/lib/$$LIB_DIR/$$platPrefix
LIBS +=  -llibzmq
zeromqLib.files = $$PWD/bin/$$LIB_DIR/$$platPrefix/*.dll

INSTALLS += zeromqLib

