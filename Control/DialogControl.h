﻿#ifndef DIALOGCONTROL_H
#define DIALOGCONTROL_H

#include <QDialog>
#include "BaseController.h"

namespace Ui {
class DialogControl;
}

class DialogControl : public QDialog
{
    Q_OBJECT

public:
    explicit DialogControl(QWidget *parent = 0);
    ~DialogControl();
    void SetController(BaseController *controller);

private slots:
    void on_btnRunSingle_clicked();

private:
    Ui::DialogControl *ui;
    BaseController *_controller=nullptr;
};

#endif // DIALOGCONTROL_H
