﻿
#ifndef CONTROLLERMANAGER_H
#define CONTROLLERMANAGER_H

#include <QObject>
#include <functional>
#include <string>

#include "BaseCommand.h"
#include "./CommandFactory/basecommandfactory.h"
#include "BaseController.h"
#include "./ControllerFactory/basecontrollerfactory.h"
#include "BaseItem.h"
#include "GlobalEnum.h"

class ControllerManager : public BaseManager
{
    Q_OBJECT
public:
     static ControllerManager& Instance();
     BaseController* GetController(QString name);

     void SetGetMeasureModeFunc(std::function<QString()> delegate);


public slots:
      void SetMeasureMode(QString measureMode);

private:
    explicit ControllerManager(QObject *parent = nullptr);
    ~ControllerManager();
    ControllerManager(const ControllerManager&) = delete;
    ControllerManager& operator=(const ControllerManager&) = delete;
    void ChangeStatus(QString mode);

signals:
    //更新当前设备状态 状态更新时触发此信号
    void UpdateDeviceStatus(RunModeZZZ status);

private:
    QString _currentMode="Idle";
    QString _measureMode;
    BaseFactory *_commandFactory=nullptr;
    // 成员变量：std::function<std::string()>
    std::function<QString()> _delegate;

    void SetMonitor(bool enable);

public slots:
    void OnPanelHandleString(QString mode_);
    void OnPanelHandle(ControlTypeZZZ mode_);

    // BaseManager interface
public:
    virtual void InitConfig() override;

    void ChangeStatus(RunModeZZZ mode);
};

#endif // CONTROLLERMANAGER_H
