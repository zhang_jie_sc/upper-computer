﻿#ifndef BASECONTROLLER_H
#define BASECONTROLLER_H

#include <QObject>
#include <QTimer>
#include "BaseCommand.h"
#include "BaseItem.h"
#include "CommandManager.h"

//控制器基类，实现连续测量和单次测量时从该基类继承
class BaseController : public BaseItem
{
    Q_OBJECT
public:
    BaseController(QObject *parent = nullptr);
    ~BaseController();

    QStringList GetCommandNames();
    BaseCommand* GetCommand(QString name);

    virtual void Execute()=0;

    void Stop();

protected:
    QList<BaseCommand*> _allCommands;
    QList<int> _weight;
    QList<QString> _cmdNames;

public slots:

    // BaseItem interface
public:
    virtual void SetConfig(QJsonObject obj) override;
    virtual QWidget *GetConfigWidget() override;

    QList<BaseCommand *> GetCommands();
protected:
    void ExecuteWrap(BaseCommand *cmd);
    void ExecuteWrap(QList<BaseCommand *> cmds);
    CommandManager *_mgr=nullptr;

private:
    void CheckWeightSum(QList<BaseCommand *> cmds, int sumWeight);
};

#endif // BASECONTROLLER_H
