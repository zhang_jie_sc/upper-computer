﻿#include "BaseFactory.h"

BaseFactory::BaseFactory()
{

}

BaseItem *BaseFactory::CreateInstance(QString concreteType)
{
    if(_map.contains(concreteType))
    {
        QObject *obj=_map[concreteType]->newInstance();
        BaseItem *ret=dynamic_cast<BaseItem*>(obj);
        if(ret==nullptr)
        {
            throw concreteType+"配置错误,请检查代码!";
        }
        return ret;
    }
    throw concreteType+"未找到该类型!";
}


