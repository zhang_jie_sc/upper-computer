﻿#pragma execution_character_set("utf-8")
#ifndef BASEFACTORY_H
#define BASEFACTORY_H
#include <QObject>
#include <QMap>
#include <QDebug>
#include "BaseItem.h"

class BaseFactory
{
public:
    BaseFactory();

    virtual BaseItem* CreateInstance(QString concreteType);

    template <typename T1>
    int Register()
    {
        int preCount=_map.count();
        const QMetaObject& meta = T1::staticMetaObject;
        _map.insert(meta.className(),&meta);

        if(preCount+1!=_map.count())
        {
            throw QString("%1注册失败，请检查!").arg(meta.className());
        }
        return _map.count();
    }

protected:
    QMap<QString,const QMetaObject*> _map;
};

#endif // BASEFACTORY_H
