﻿#include "BaseItem.h"

BaseItem::BaseItem(QObject *parent) : QObject(parent)
{

}

QString BaseItem::Name()
{
    return _name;
}

void BaseItem::SetName(QString name)
{
    _name=name;
}

QJsonObject BaseItem::Config()
{
    return _obj;
}

void BaseItem::SetConfig(QJsonObject obj)
{
    _obj=obj;
}

void BaseItem::SetOtherConfig(QJsonObject other)
{
    _other=other;
}

bool BaseItem::Init()
{
    return true;
}

QWidget *BaseItem::GetConfigWidget()
{
    throw QString("%1 未实现配置界面").arg(_name);
}

QSharedPointer<QWidget> BaseItem::GetConfigWidgetZZZ()
{
    return nullptr;
}

