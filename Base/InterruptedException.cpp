﻿#include "InterruptedException.h"

void InterruptedException::raise() const
{
    throw *this;
}

InterruptedException *InterruptedException::clone() const
{
    return new InterruptedException();
}
